#!/usr/bin/bash

MPLUSVER="063"
UMEVER="670"
UMEPLUSVER="20180604"

rm -rf mplus-TESTFLIGHT-${MPLUSVER}
tar xf mplus-TESTFLIGHT-${MPLUSVER}.tar.xz
rm -rf umefont_${UMEVER}
tar xf umefont_${UMEVER}.tar.xz
rm -rf umeplus-cl-fonts-${UMEPLUSVER}
rm umeplus-cl-fonts-${UMEPLUSVER}.tar.xz

rm umeplus-cl-fonts-dev/docs-*/*
rm umeplus-cl-fonts-dev/fontforge-scripts-umeplus*/*.ttf

cp update-umeplus-cl.sh umeplus-cl-fonts-dev/

cp mplus-TESTFLIGHT-${MPLUSVER}/mplus-1m-regular.ttf umeplus-cl-fonts-dev/fontforge-scripts-umeplus-cl/
cp mplus-TESTFLIGHT-${MPLUSVER}/mplus-1c-regular.ttf umeplus-cl-fonts-dev/fontforge-scripts-umeplus-clp/
cp mplus-TESTFLIGHT-${MPLUSVER}/{LICENSE_*,README_*} umeplus-cl-fonts-dev/docs-mplus/

chmod 644 umefont_${UMEVER}/*
cp umefont_${UMEVER}/ume-tgc4.ttf umeplus-cl-fonts-dev/fontforge-scripts-umeplus-cl/
cp umefont_${UMEVER}/ume-pgc4.ttf umeplus-cl-fonts-dev/fontforge-scripts-umeplus-clp/
cp umefont_${UMEVER}/license.html umeplus-cl-fonts-dev/docs-ume/

cd umeplus-cl-fonts-dev/fontforge-scripts-umeplus-cl/
fontforge -script fix-mplus-1m-regular.pe
fontforge -script import-mplus.pe
cd -

cd umeplus-cl-fonts-dev/fontforge-scripts-umeplus-clp/
fontforge -script fix-mplus-1c-regular.pe
fontforge -script import-mplus.pe
cd -

mv umeplus-cl-fonts-dev/fontforge-scripts-umeplus*/umeplus-*.ttf umeplus-cl-fonts-dev/
rm umeplus-cl-fonts-dev/fontforge-scripts-umeplus*/*.ttf

mv umeplus-cl-fonts-dev umeplus-cl-fonts-${UMEPLUSVER}
tar Jcvf umeplus-cl-fonts-${UMEPLUSVER}.tar.xz umeplus-cl-fonts-${UMEPLUSVER}
mv umeplus-cl-fonts-${UMEPLUSVER} umeplus-cl-fonts-dev
